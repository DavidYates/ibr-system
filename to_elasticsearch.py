#/usr/bin/python
#Usage: ./to_elasticsearch.py <packet file> <number of packets> <server to stream to> <darknet name>
from scapy.all import *
from subprocess import call
import sys, datetime, json

def create_index():
    """
    Create index for loading packet data into.
    """
    index = {
            "mappings" : {
                "_default_" : {
                    "properties" : {
                        "date" : {"type": "string", "index" : "not_analyzed" },
                        "source_address" : {"type": "string" },
                        "destination_address" : { "type" : "string" },
                        "source_port" : { "type" : "integer" },
                        "destination_port" : { "type" : "integer" },
                        "protocol" : { "type" : "string" },
                        "source_darknet" : { "type" : "string" }
                        }
                    }
                }
            }
    call(["curl", "-XPUT", server, "-d", json.dumps(index)])

def stream_to_es(packets, server):
    """
    Process pcap data and send it to Elasticsearch

    packets -- scapy packets object 
    server -- Elasticsearch server URL
    """
    create_index()

    count = 0
    for packet in packets:
        pdict = {}

        pdict["source_darknet"] = sys.argv[4]
        pdate = datetime.datetime.fromtimestamp(packet.time)
        pdict["date"] = str(pdate)

        if packet.haslayer(IP):
            pdict["source_address"] = packet[IP].src
            pdict["destination_address"] = packet[IP].dst
        else:
            pdict["source_address"] = None
            pdict["destination_address"] = None

        if packet.haslayer(TCP):
            pdict["protocol"] = "TCP"
            pdict["source_port"] = packet[TCP].sport
            pdict["destination_port"] = packet[TCP].dport
        elif packet.haslayer(UDP):
            pdict["protocol"] = "UDP"
            pdict["source_port"] = packet[UDP].sport
            pdict["destination_port"] = packet[UDP].dport
        elif packet.haslayer(ICMP):
            pdict["protocol"] = "ICMP"
            pdict["source_port"] = None
            pdict["destination_port"] = None
        else:
            pdict["protocol"] = None
            pdict["source_port"] = None
            pdict["destination_port"] = None

        count += 1
        if count % 500000 == 0:
            print "{0} done so far.".format(count)

        call(["curl", "-XPOST", "{0}{1}/".format(server, pdict["protocol"]), "-d", json.dumps(pdict)])

def main(argv):
    packets = rdpcap(argv[0], int(argv[1]))
    stream_to_es(packets, argv[2])

if __name__ == '__main__':
    main(sys.argv[1:])

#!/usr/bin/python

from elasticsearch import Elasticsearch
from pandas import DataFrame
from pandas import Series
import json

class ESInterface:
    """
    Object for querying Elasticsearch
    """

    def __init__(self, url="localhost:9200", index="packets"):
        """
        Instantiate ESInterface

        url -- the Elasticsearch instance (defaults to localhost)
        index -- the index to work with (defaults to the packets index)
        """
        self.es = Elasticsearch(url)
        self.index = index


    def search(self, fil={ "match_all": {} }, size=10):
        """
        Build and run a search for top terms based on a given filter.

        fil -- a dictionary filter of the form { "field1":"value1" ... "fieldn":"valuen" } Multiple acceptable values for a single field can be given in a list.
        """
        results = self.es.search(index=self.index, body=self._filter_builder(fil), size=size)
        return DataFrame([results["hits"]["hits"][n]["_source"] for n in range(0, len(results["hits"]["hits"]))])

    def date_histogram(self, fil={ "match_all": {} }, interval="day"):
        """
        Get a set of buckets to create a date histogram with

       fil -- a dictionary filter of the form { "field1":"value1" ... "fieldn":"valuen" } Multiple acceptable values for a single field can be given in a list.
        interval -- the bucket interval (day, week, minute, etc)
        """

        buckets = self._query_builder(self._date_histogram_query(interval), fil)
        buckets.columns = ["count", "timestamp", "timestamp_string"]
        return buckets

    def top_terms(self, field, fil={ "match_all": {} }, size=10):
        """
        Find the top terms in a given field

        field -- a field in the elasticsearch index documents
       fil -- a dictionary filter of the form { "field1":"value1" ... "fieldn":"valuen" } Multiple acceptable values for a single field can be given in a list.
        size -- the number of results to return
        """

        topterms = self._query_builder(self._top_terms_query(field, size), fil)
        if not topterms.empty:
            topterms.columns = ["count", field]
        return topterms

    def _date_histogram_query(self, interval="day"):
        """
        PRIVATE
        Builds a query for fetching buckets to create a date histogram

        interval -- the bucket interval (day, week, minute, etc)
        """
        dh_dict = { "aggregations": {
                        "thing": {
                            "date_histogram" : {
                                "field" : "date",
                                "interval" : interval
                            }
                        }
                    }
                }
        return dh_dict

    def _top_terms_query(self, field, size=10):
        """
        PRIVATE
        Builds a query for finding the top terms in a given field

        field -- a field in the elasticsearch index documents
        size -- the number of results to return
        """
        term_dict = { "aggregations" : {
                        "thing" : {
                            "terms" : {
                                "field" : field,
                                "size" : size
                                }
                            }
                        }
                }
        return term_dict

    def _query_builder(self, que, fil={ "match_all" : {} }, size=10):
        """
        PRIVATE
        Build and run a query.

        que -- a _date_histogram_query or _top_terms_query query
        fil -- a dictionary filter of the form { "field1":"value1" ... "fieldn":"valuen" } Multiple acceptable values for a single field can be given in a list.
        size -- the number of results to return
        """
        querydict = dict(self._filter_builder(fil).items() + que.items())
        query = json.dumps(querydict)
        results = self.es.search(index=self.index, body=query)['aggregations']['thing']

        if results.has_key('buckets'):
            results = results['buckets']
        else:
            results = results['terms']

        return DataFrame(results)

    def _filter_builder(self, fils):
        """
        Given a dictionary filter, builds an elasticsearch filter

        fils -- a dictionary filter of the form { "field1":"value1" ... "fieldn":"valuen" } Multiple acceptable values for a single field can be given in a list.
        """

        filt = fils.copy()
        f = []
        if filt.has_key("match_all"):
            f = filt
        else:
            if filt.has_key("protocol"): #bit of a hack, but it saves users from having to use _type
                filt["_type"] = filt["protocol"]
                filt.pop("protocol")
            for k,v in filt.iteritems():
                f.append({ "term" : { str(k):v } })            

        filter_dict = { "query" : {
                "filtered" : {
                    "filter": {
                        "bool" : {
                            "must": f
                        }
                    }
                }
            }
        }
        return filter_dict

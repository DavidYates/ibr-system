#A System for Characterising Internet Background Radiation

This is a proof of concept system for performing network traffic analysis using Elasticsearch, pandas and Matplotlib. It includes a script that loads pcap data into an Elasticsearch instance, and a number of scripts for drawing graphs of that data.

##Requirements:

* Python 2.7
* Elasticsearch 1.0 or above
* scapy
* OpenCV for Python
* pandas
* Matplotlib

This code was written towards the completion of a Computer Science Honours degree at Rhodes University. See http://www.cs.ru.ac.za/research/g11y1408/ for more information.

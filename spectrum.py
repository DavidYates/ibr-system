#!/usr/bin/python

import numpy as np
import pandas as pd
import cv2
from esinterface import ESInterface
import dateutil.parser
import datetime
import time

def draw_dport_spectrum(esi, dport, protocol, darknet):
    """
    Draw a spectrum for destination address packets received on a given port

    esi -- ESInterface object
    dport -- destination port to look at
    protocol -- packet type to look at (UDP or TCP)
    darknet -- darknet to look at
    """
    topaddresses = esi.top_terms("source_address", { "source_darknet": darknet, "destination_port": dport, "protocol" : protocol }, size=10)

    #CSV file
    from ibrutils import setup_filepath
    filename = "Spectrum-CSV-{2}{1}/{0}/spectrum-dn{0}dp{1}.csv".format(darknet,dport,protocol)
    setup_filepath(filename)
    csv = open(filename, 'w')
    csv.write("Darknet, Destination Port, Source Address, Destination Address\n")

    for sa in topaddresses.iterrows():
        #Data gathering
        daddresses = esi.search({"source_darknet": darknet, "destination_port": dport, "source_address": sa[1]["source_address"], "protocol": protocol }, size=sa[1]["count"])
        daddresses['date'] = daddresses['date'].apply(dateutil.parser.parse)
        daddresses = daddresses.sort(columns="date")
        dlist = [daddresses["destination_address"].ix[b] for b in range(0, daddresses.shape[0])]
        #Drawing
        x = 0
        img = np.zeros((255, min(len(dlist),1000), 3), np.uint8)
        for da in dlist:
            csv.write("{0}, {1}, {2}, {3}\n".format(darknet, dport, sa[1]["source_address"], da))
            col = [int(d) for d in da.split(".")] #get colour from final segment
            ln = cv2.line(img, (x, 0), (x, 255), (11, 11, col[3]), 1)
            x += 1
            if x > 1000:
                break

        filename = "Spectrums-{3}{1}/{0}/dn{0}dp-{3}{1}sa{2}-spectrum.png".format(darknet, dport, sa[1]["source_address"], protocol)
        setup_filepath(filename)
        cv2.imwrite(filename, img)


def main():
    for darknet in ["196-A", "196-B", "196-C", "146", "155"]:
        for dport in [("1434","UDP"), ("22", "TCP"), ("3389", "TCP")]:
            draw_dport_spectrum(ESInterface(), dport[0], dport[1], darknet)

if __name__ == "__main__":
    start_time = time.time()
    main()
    print "{0} seconds taken".format(time.time() - start_time)

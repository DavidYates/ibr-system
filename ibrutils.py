#Some general utility functions that don't fit in anywhere

import os

def setup_filepath(filename):
    if "/" in filename:
        path = filename[:filename.rfind("/")]

        if not os.path.exists(path):
            os.makedirs(path)

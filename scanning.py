#!/usr/bin/python

import numpy as np
import pandas as pd
from esinterface import ESInterface
from graph import Graph
from elasticsearch import Elasticsearch
import dateutil.parser
import time

def draw_scanning_graphs(esi, dport, protocol, darknet):
    """
    Draw graphs to determine the likelihood of scanning activity

    esi -- an ESInterface to use
    dport -- the destination port to look at
    protocol -- packet type to look at (TCP or UDP)
    darknet -- the darknet to look at
    """

    topaddresses = esi.top_terms("source_address", { "source_darknet": darknet, "destination_port": dport, "protocol": protocol }, size=10)
    for sa in topaddresses.iterrows():
        #Draw graphs for each source
        overall = esi.date_histogram({ "source_darknet": darknet, "destination_port": dport }, "day")
        Graph.save_graph(overall, "ScanningGraphs-{3}{1}/{0}/dn{0}dp-{3}{1}sa{2}-overall-scanning-graph.png".format(darknet, dport, sa[1]["source_address"], protocol))
        #Data gathering
        daddresses = esi.search({"source_darknet": darknet, "destination_port": dport, "source_address": sa[1]["source_address"]}, size=10)
        dlist = [daddresses["destination_address"].ix[b] for b in range(0, daddresses.shape[0])]
        #Draw individual graphs for each source and destination
        for da in dlist:
            graphables = esi.date_histogram({ "source_darknet": darknet, "destination_port": dport, "source_address": sa[1]["source_address"], "destination_address": da }, "day")
            Graph.save_graph(graphables, "ScanningGraphs-{4}{1}/{0}/dn{0}dp-{4}{1}sa{2}da{3}-graph.png".format(darknet, dport, sa[1]["source_address"], da, protocol))

def main():
    for d in ["146", "155", "196-A", "196-B", "196-C"]:
            for p in [("22","TCP"), ("3389","TCP"), ("1434","UDP")]:
                draw_scanning_graphs(ESInterface(), p[0], p[1], d)

if __name__ == "__main__":
    start_time = time.time()
    main()
    print "{0} seconds taken".format(time.time() - start_time)

#!/usr/bin/python

from esinterface import ESInterface
from pandas import DataFrame
from pandas import Series
import socket
import matplotlib.pyplot as plt
import matplotlib.dates as md
import time

class Graph:
    """
    Object for drawing graphs
    """

    @staticmethod
    def save_graph(date_histograms, filename="graph.png", legend=[]):
        """
        Save Matplotlib graphs of date histograms from elasticsearch

        date_histogram -- the date histogram or list of date date histograms (produced by an ESInterface) to graph
        legend -- list of string names for the legend (defaults to an empty list and no legend)
        """

        #Make plots
        if type(date_histograms) == list:
            for d in date_histograms:
                tmp = zip(*d.get_values())
                ax = Series(tmp[0], [t[:10] for t in tmp[2]]).plot()
        else:
            tmp = zip(*date_histograms.get_values())
            ax = Series(tmp[0], [t[:10] for t in tmp[2]]).plot()


        #Set up axes
        plt.xlabel("Time")
        plt.ylabel("Packets")
        plt.xticks(rotation=45)
        plt.subplots_adjust(bottom=.2)

        #Make legend
        if len(legend) >= 2:
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            plt.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5))

        #Save figure, setting up paths if necessary
        from ibrutils import setup_filepath
        setup_filepath(filename)
        plt.savefig(filename)
        plt.clf()

def main():
    esi = ESInterface()
    darknets = ["146", "155", "196-A", "196-B", "196-C"]
    legend = ["146", "155", "196-A", "196-B", "196-C"]
    for protocol in ["UDP", "TCP"]:
        dports = esi.top_terms("destination_port", { "protocol":protocol }, size=10)
        sports = esi.top_terms("source_port", { "protocol":protocol }, size=10)
        for p in sports.source_port:
            graphables = []
            for d in darknets:
                graphables.append(esi.date_histogram({ "source_port":p, "source_darknet":d, "protocol":protocol }, "day"))
            Graph.save_graph(graphables, "sp-{1}{0}-graph.png".format(p,protocol), legend)
        for p in dports.destination_port:
            graphables = []
            for d in darknets:
                graphables.append(esi.date_histogram({ "destination_port":p, "source_darknet":d, "protocol":protocol }, "day"))
            Graph.save_graph(graphables, "dp-{1}{0}-graph.png".format(p, protocol), legend)

if __name__ == "__main__":
    start_time = time.time()
    main()
    print "{0} seconds taken".format(time.time() - start_time)
